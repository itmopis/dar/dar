openapi: 3.0.3
info:
  title: Lab 1 - OpenAPI 3.0 - City
  version: 1.0.11
servers:
  - url: https://localhost:????/api/v1

tags:
  - name: crud
    description: CRUD operations

paths:
  /cities:
    get:
      summary: Get a list of cities
      tags:
        - crud
      parameters:
        - $ref: '#/components/parameters/FilterParam'
        - $ref: '#/components/parameters/SortParam'
        - name: limit
          description: "Кол-во элементов на странице"
          in: query
          schema:
            type: integer
        - name: offset
          description: "Сколько записей пропустить"
          in: query
          schema:
            type: integer
          required: False
        - name: page
          description: "Номер страницы"
          in: query
          schema:
            type: integer
      responses:
        '200':
          $ref: '#/components/responses/200CityList'
        '400':
          $ref: '#/components/responses/400Response'

    post:
      summary: Create a new city
      tags:
        - crud
      requestBody:
        required: true
        content:
          application/xml:
            schema:
              $ref: '#/components/schemas/City'

      responses:
        '200':
          $ref: '#/components/responses/200City'
        '400':
          $ref: '#/components/responses/404Response'

  /cities/{cityId}:
    get:
      summary: Get city
      tags:
        - crud
      parameters:
        - $ref: '#/components/parameters/CityIdParam'
      responses:
        '200':
          $ref: '#/components/responses/200City'
        '400':
          $ref: '#/components/responses/400Response'
        '404':
          $ref: '#/components/responses/404Response'

    put:
      summary: Update a city
      tags:
        - crud
      parameters:
        - $ref: '#/components/parameters/CityIdParam'
        - name: city
          in: query
          required: true
          schema:
            $ref: '#/components/schemas/City'
      responses:
        '200':
          $ref: '#/components/responses/200City'
        '400':
          $ref: '#/components/responses/400Response'
        '404':
          $ref: '#/components/responses/404Response'

    delete:
      summary: Delete a city
      tags:
        - crud
      parameters:
        - $ref: '#/components/parameters/CityIdParam'
      responses:
        '204':
          $ref: '#/components/responses/204Ok'
        '404':
          $ref: '#/components/responses/404Response'

  /cities/governon/filter:
    get:
      summary: Filter cities by governon
      tags:
        - crud
      parameters:
        - name: governon
          in: query
          required: true
          schema:
            $ref: '#/components/schemas/Human'
        - name: limit
          in: query
          schema:
            type: integer
        - name: offset
          in: query
          schema:
            type: integer
        - name: page
          in: query
          schema:
            type: integer
      responses:
        '200':
          $ref: '#/components/responses/200CityList'
        '400':
          $ref: '#/components/responses/400Response'


  /cities/sea-level/{metersAboveSea}:
    delete:
      summary: Delete cities with equivalent level above sea
      tags:
        - crud
      parameters:
        - $ref: '#/components/parameters/MetersAboveSeaParam'
      responses:
        '204':
          $ref: '#/components/responses/204Ok'
        '400':
          $ref: '#/components/responses/400Response'
        '404':
          $ref: '#/components/responses/404Response'

  /cities/government/min:
    get:
      summary: Get city with the min government type
      tags:
        - crud
      parameters:
        - name: governmentType
          in: query
          required: true
          schema:
            $ref: '#/components/schemas/Government'
      responses:
        '200':
          $ref: '#/components/responses/200City'
        '400':
          $ref: '#/components/responses/400Response'

  /cities/governon/count:
    get:
      summary: Count cities where field governon is lower than input
      tags:
        - crud
      parameters:
        - name: governon
          in: query
          required: true
          schema:
            $ref: '#/components/schemas/Human'
      responses:
        '200':
          description: The amount of cities whith specific governon
          content:
            application/xml:
              schema:
                type: integer
                format: int32
                example: 15
                xml:
                  name: sum
        '400':
          $ref: '#/components/responses/404Response'


components:
  parameters:
    FilterParam:
      name: filter
      description: "Поля для фильтрации записей по совпадению."
      in: query
      schema:
        type: array
        items:
          type: object
          required:
            - field
            - value
          properties:
            field:
              type: string
              example: name
            value:
              type: string
              example: detroid

    SortParam:
      name: sort
      description: "Поля по которым будет сортировка. Чем раньше элемент в массиве, тем больше у него приоритет сортировки."
      in: query
      schema:
        type: array
        items:
          type: object
          required:
            - field
            - order
          properties:
            field:
              type: string
              example: name
            order:
              type: string
              enum:
                - asc
                - desc

    CityIdParam:
      name: cityId
      in: path
      required: true
      schema:
        type: integer
        minimum: 1

    MetersAboveSeaParam:
      name: metersAboveSea
      in: path
      required: true
      schema:
        type: integer
        items:
          $ref: '#/components/schemas/City'



  schemas:
    City:
      xml:
        name: City
      type: object
      properties:
        id:
          readOnly: true
          nullable: false
          type: integer
          format: 'int32'
          minimum: 1
          example: 100
        name:
          nullable: false
          type: string
          minLength: 1
          example: "cityName"
        coordinates:
          $ref: '#/components/schemas/Coordinates'
        creationDate:
          readOnly: true
          nullable: false
          type: string
          format: "date"
        area:
          type: number
          format: float
          minimum: 0
          exclusiveMinimum: true
        population:
          nullable: false
          type: integer
          format: 'int32'
          minimum: 1
          example: 100
        metersAboveSeaLevel:
          type: integer
          format: 'int64'
          minimum: 1
          example: 100
        carCode:
          nullable: false
          type: integer
          format: 'int64'
          minimum: 1
          maximum: 1000
          example: 100
        climate:
          $ref: '#/components/schemas/Climate'
        government:
          $ref: '#/components/schemas/Government'
        governor:
          $ref: '#/components/schemas/Human'

    Coordinates:
      type: object
      properties:
        x:
          nullable: false
          type: number
          format: 'double'
          example: 100
        y:
          type: integer
          format: 'int32'
          maximum: 109
          example: 100

    Human:
      nullable: false
      type: object
      properties:
        age:
          nullable: false
          type: number
          format: 'double'
          example: 100

    Climate:
      type: string
      nullable: true
      enum:
        - 'STEPPE'
        - 'TUNDRA'
        - 'DESERT'

    Government:
      type: string
      enum:
        - 'IDEOCRACY'
        - 'KLEPTOCRACY'
        - 'NOOCRACY'
        - 'STRATOCRACY'
        - 'JUNTA'


  responses:
    200City:
      description: City
      content:
        application/xml:
          schema:
            $ref: '#/components/schemas/City'


    204Ok:
      description: Operation is successfully done

    200CityList:
      description: List of cities
      content:
        application/xml:
          schema:
            type: array
            items:
              $ref: '#/components/schemas/City'
            xml:
              name: Cities
              wrapped: true

    400Response:
      description: Error
      content:
        application/xml:
          schema:
            xml:
              name: Error
            type: object
            properties:
              code:
                description: 'http code answer'
                type: integer
                nullable: false
                example: 400
              description:
                description: 'short description of the code'
                type: string
                nullable: false
                example: 'Bad request'
              reason:
                description: 'short description of the reason if the http code 4XX or 5XX'
                type: string
                example: 'InvalidParametersException{ field with type a number must be number!).}'

    404Response:
      description: Error
      content:
        application/xml:
          schema:
            xml:
              name: Error
            type: object
            properties:
              code:
                description: 'http code answer'
                type: integer
                nullable: false
                example: 404
              description:
                description: 'short description of the code'
                type: string
                nullable: false
                example: 'Not found'
              reason:
                description: 'short description of the reason if the http code 4XX or 5XX'
                type: string
                example: 'Объект с id 6 типа город не найден.'



